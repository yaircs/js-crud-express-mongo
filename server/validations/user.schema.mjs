import * as yup from 'yup';


const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

export const createOrPutSchema = yup.object().shape({
  first_name: yup.string().required().min(2).max(20),
  last_name: yup.string().required().min(2).max(20),
  email: yup.string().email().required(),
  phone: yup.string().max(13).min(7).required().matches(phoneRegExp),
});

export const patchSchema = yup.object().shape({
  first_name: yup.string().min(2).max(20),
  last_name: yup.string().min(2).max(20),
  email: yup.string().email(),
  phone: yup.string().max(13).min(7).matches(phoneRegExp),
}).test('at-least-one-field', "you must provide at least one field", value =>
!!(value.first_name || value.last_name || value.email || value.phone)
);