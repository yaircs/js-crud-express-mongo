import { createOrPutSchema, patchSchema } from '../validations/user.schema.mjs';

export function validateCreateOrPut(req, res, next) {
    createOrPutSchema.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}

export function validatePatch(req, res, next) {
    patchSchema.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}